<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Odm\Filter\ExistsFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use App\State\Provider\InfoProvider;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

#[ORM\HasLifecycleCallbacks]
#[ApiResource(operations: [
    new Get(
        uriTemplate: '/info/{uuid}',
        openapiContext: [
            'summary' => 'Retrieves geonetwork information with its uuid'
        ],
        normalizationContext: ['groups' => ['getInfoDownload']] )
], provider: InfoProvider::class)]
class Info
{
    /**
     * @var Uuid
     */
    #[ApiProperty(identifier: true)]
    #[Groups(['getInfoDownload'])]
    private Uuid $uuid;

    #[Groups(['getInfoDownload'])]
    private string $metadataTitle;

    #[Groups(['getInfoDownload'])]
    private string $extractMap;

    #[Groups(['getInfoDownload'])]
    private string $spatialRepresentationType;

    #[Groups(['getInfoDownload'])]
    private array $layers;

    #[Groups(['getInfoDownload'])]
    private string $cgu_message;

    #[Groups(['getInfoDownload'])]
    private bool $cgu_display;
    #[Groups(['getInfoDownload'])]
    private ?ResourceConstraints $resourceConstraints;

    /**
     * @return Uuid
     */
    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    /**
     * @param Uuid $uuid
     * @return Info
     */
    public function setUuid(Uuid $uuid): Info
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetadataTitle(): string
    {
        return $this->metadataTitle;
    }

    /**
     * @param string $metadataTitle
     * @return Info
     */
    public function setMetadataTitle(string $metadataTitle): Info
    {
        $this->metadataTitle = $metadataTitle;
        return $this;
    }

    /**
     * @return string
     */
    public function getExtractMap(): string
    {
        return $this->extractMap;
    }

    /**
     * @param string $extractMap
     * @return Info
     */
    public function setExtractMap(string $extractMap): Info
    {
        $this->extractMap = $extractMap;
        return $this;
    }

    /**
     * @return string
     */
    public function getSpatialRepresentationType(): string
    {
        return $this->spatialRepresentationType;
    }

    /**
     * @param string $spatialRepresentationType
     * @return Info
     */
    public function setSpatialRepresentationType(string $spatialRepresentationType): Info
    {
        $this->spatialRepresentationType = $spatialRepresentationType;
        return $this;
    }

    /**
     * @return array
     */
    public function getLayers(): array
    {
        return $this->layers;
    }

    /**
     * @param array $layers
     * @return Info
     */
    public function setLayers(array $layers): Info
    {
        $this->layers = $layers;
        return $this;
    }

    /**
     * @return string
     */
    public function getCguMessage(): string
    {
        return $this->cgu_message;
    }

    /**
     * @param string $cgu_message
     * @return Info
     */
    public function setCguMessage(string $cgu_message): Info
    {
        $this->cgu_message = $cgu_message;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCguDisplay(): bool
    {
        return $this->cgu_display;
    }

    /**
     * @param bool $cgu_display
     * @return Info
     */
    public function setCguDisplay(bool $cgu_display): Info
    {
        $this->cgu_display = $cgu_display;
        return $this;
    }

    /**
     * @return ResourceConstraints|null
     */
    public function getResourceConstraints(): ?ResourceConstraints
    {
        return $this->resourceConstraints;
    }

    /**
     * @param ResourceConstraints|null $resourceConstraints
     * @return Info
     */
    public function setResourceConstraints(?ResourceConstraints $resourceConstraints): Info
    {
        $this->resourceConstraints = $resourceConstraints;
        return $this;
    }

}