<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Metadata\ApiFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    operations: [
        new Get(openapiContext: [
            'summary' => 'Retrieves all statuses'
        ],
            normalizationContext: ['groups' => ['getState']] )
    ]
)]
#[ORM\Table(name: 'status', schema: 'telechargement')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Status
{
    #[ORM\Id]
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private int $id;

    #[ORM\Column(name: 'name', type: 'string', length: 128, nullable: false)]
    #[Groups(['read','getState','getDownload'])]
    #[Assert\NotNull]
    private string $name;

    #[ORM\Column(name: 'description', type: 'string', length: 128, nullable: false)]
    #[Groups(['read','getState'])]
    private ?string $description = null;

    /**
     * @var Collection
     */
    #[ORM\OneToMany(mappedBy: 'status', targetEntity: Download::class, orphanRemoval: false)]
    private Collection $downloads;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return Status
     */
    public function setName(?string $name): Status
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return Status
     */
    public function setDescription(?string $description): Status
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getDownloads(): Collection
    {
        return $this->downloads;
    }

    /**
     * @param Collection $downloads
     * @return Status
     */
    public function setDownloads(Collection $downloads): Status
    {
        $this->downloads = $downloads;
        return $this;
    }


}