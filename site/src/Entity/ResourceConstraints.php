<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

class ResourceConstraints
{
    #[Groups(['getInfoDownload'])]
    private ?array $useLimitation;

    #[Groups(['getInfoDownload'])]
    private ?string $otherConstraints;

    public function getUseLimitation(): ?array
    {
        return $this->useLimitation;
    }

    public function setUseLimitation(?array $useLimitation): void
    {
        $this->useLimitation = $useLimitation;
    }

    public function getOtherConstraints(): ?string
    {
        return $this->otherConstraints;
    }

    public function setOtherConstraints(?string $otherConstraints): ResourceConstraints
    {
        $this->otherConstraints = $otherConstraints;
        return $this;
    }





}