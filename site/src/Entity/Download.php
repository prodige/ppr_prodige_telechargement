<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use App\Controller\DownloadFileController;
use App\Entity\Trait\TimestampsTrait;
use App\State\Processor\DownloadProcessor;
use App\State\Provider\StateProvider;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[Post(openapiContext: [
    'summary' => 'Allows you to retrieve a dataset'
], denormalizationContext: ['groups' => ['write']],
    processor: DownloadProcessor::class)]
#[ApiResource(operations: [
    new Get(
        uriTemplate: '/download/{id}/file',
        outputFormats: ['octet'],
        requirements: ['id' => '\d+'],
        controller: DownloadFileController::class,
        openapiContext: [
            'summary' => 'Download data Archive',
            'responses' => [
                200 => [
                    'description' => 'Download data Archive',
                ]
            ]
        ],
        normalizationContext: ['groups' => ['getFile']],
        name: 'download_file'
    ),
    new Delete(
        processor: DownloadProcessor::class
    ),
    new GetCollection(paginationEnabled: false, normalizationContext: [
        'groups' => ['getDownload'],
        'skip_null_values' => false
    ]
    ),
    new Get()
]
)]
#[ApiResource(operations: [
    new Get(
        uriTemplate: '/download/{id}/state',
        requirements: ['id' => '\d+'],
        openapiContext: [
            'summary' => 'Shows data status'
        ],
        normalizationContext: ['groups' => ['getState']],
        provider: StateProvider::class
    )
]
)]
#[ApiFilter(SearchFilter::class, properties: [
    'status.id' => 'exact'
])]
#[ORM\Table(name: 'download', schema: 'telechargement')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Download
{
    use TimestampsTrait;

    #[ORM\Id]
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['write', 'readStatus', 'getFile', 'getDownload'])]
    private int $id;


    #[ORM\Column(name: 'metadata_uuid', type: 'uuid', nullable: false)]
    #[Groups(['write', 'getDownload'])]
    #[Assert\NotNull]
    private Uuid $metadataUuid;

    #[ORM\Column(name: 'user_id', type: 'integer', nullable: false)]
    #[Groups(['write', 'getDownload'])]
    private int $userId;

    #[ORM\Column(name: 'user_mail', type: 'string', length: 128, nullable: false)]
    #[Groups(['write'])]
    #[Assert\Email]
    #[ApiProperty(
        required: false, example: "user@example.com", types: EmailType::class
    )]
    private ?string $userMail = null;

    #[ORM\Column(name: 'direct', type: 'boolean', nullable: false)]
    #[Groups(['write', 'getDownload'])]
    #[Assert\NotNull]
    #[ApiProperty(
        required: true, example: true, types: "boolean"
    )]
    private bool $direct;

    #[ORM\Column(name: 'format', type: 'string', length: 128, nullable: false)]
    #[Groups(['write', 'getDownload'])]
    #[Assert\NotNull]
    #[ApiProperty(
        required: true, example: "json", types: "string"
    )]
    private ?string $format = null;

    #[ORM\Column(name: 'srs', type: 'string', length: 128, nullable: false)]
    #[Groups(['write'])]
    #[Assert\NotNull]
    #[ApiProperty(
        required: true, example: "EPSG:2154", types: "string"
    )]
    private ?string $srs = null;

    #[ORM\Column(name: 'extract', type: 'json', nullable: false)]
    #[Groups(['write'])]
    #[ApiProperty(
        required: true,
        example: [
            "type" => "list",
            "area_type_id" => 2,
            "area_id" => ["2", "3"]
        ],
        types: "array"
    )]
    private array $extract = [];

    #[ORM\Column(name: 'created_at', type: 'datetime', nullable: false)]
    #[Groups(['getDownload'])]
    private ?DateTime $createdAt = null;

    #[ORM\ManyToOne(targetEntity: Status::class, inversedBy: 'downloads')]
    #[ORM\JoinColumn(name: 'status_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(['getState', 'getDownload'])]
    private ?Status $status = null;

    #[ORM\Column(name: 'file', type: 'string', length: 256, nullable: true)]
    #[Groups(['getFile'])]
    private ?string $file = null;

    #[ORM\Column(name: 'response', type: 'text', nullable: true)]
    #[Groups(['getState', 'getDownload'])]
    private ?string $response = null;

    #[ORM\Column(name: 'queue_name', type: 'string', length: 256, nullable: true)]
    #[Groups(['getDownload'])]
    private ?string $queueName = null;

    #[ORM\Column(name: 'queue_id', type: 'integer', nullable: true)]
    private ?int $queueId;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Uuid|null
     */
    public function getMetadataUuid(): ?Uuid
    {
        return $this->metadataUuid;
    }

    /**
     * @param Uuid|null $metadataUuid
     * @return Download
     */
    public function setMetadataUuid(?Uuid $metadataUuid): Download
    {
        $this->metadataUuid = $metadataUuid;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return Download
     */
    public function setUserId(int $userId): Download
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserMail(): ?string
    {
        return $this->userMail;
    }

    /**
     * @param string|null $userMail
     * @return Download
     */
    public function setUserMail(?string $userMail): Download
    {
        $this->userMail = $userMail;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isDirect(): ?bool
    {
        return $this->direct;
    }


    /**
     * @param bool|null $direct
     * @return Download
     */
    public function setDirect(?bool $direct): Download
    {
        $this->direct = $direct;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFormat(): ?string
    {
        return $this->format;
    }

    /**
     * @param string|null $format
     * @return Download
     */
    public function setFormat(?string $format): Download
    {
        $this->format = $format;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSrs(): ?string
    {
        return $this->srs;
    }

    /**
     * @param string|null $srs
     * @return Download
     */
    public function setSrs(?string $srs): Download
    {
        $this->srs = $srs;
        return $this;
    }

    /**
     * @return array
     */
    public function getExtract(): array
    {
        return $this->extract;
    }

    /**
     * @param array $extract
     * @return Download
     */
    public function setExtract(array $extract): Download
    {
        $this->extract = $extract;
        return $this;
    }

    /**
     * @return Status|null
     */
    public function getStatus(): ?Status
    {
        return $this->status;
    }

    /**
     * @param Status|null $status
     * @return Download
     */
    public function setStatus(?Status $status): Download
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFile(): ?string
    {
        return $this->file;
    }

    /**
     * @param string|null $file
     * @return Download
     */
    public function setFile(?string $file): Download
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getResponse(): ?string
    {
        return $this->response;
    }

    /**
     * @param string|null $response
     * @return Download
     */
    public function setResponse(?string $response): Download
    {
        $this->response = $response;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getQueueName(): ?string
    {
        return $this->queueName;
    }

    /**
     * @param string|null $queueName
     * @return Download
     */
    public function setQueueName(?string $queueName): Download
    {
        $this->queueName = $queueName;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getQueueId(): ?int
    {
        return $this->queueId;
    }

    /**
     * @param int|null $queueId
     * @return Download
     */
    public function setQueueId(?int $queueId): Download
    {
        $this->queueId = $queueId;
        return $this;
    }


}
