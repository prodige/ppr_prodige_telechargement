<?php

namespace App\Handler;

use App\Entity\Download;
use App\Message\DiffereImportMessage;
use App\Service\ImportQueueService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler( fromTransport: 'async_priority_low', priority: 5)]
class DiffereImportHandler
{
    private EntityManagerInterface $entityManager;
    private ImportQueueService $queueService;

    public function __construct(EntityManagerInterface $entityManager, ImportQueueService $queueService)
    {
        $this->entityManager = $entityManager;
        $this->queueService = $queueService;
    }

    public function __invoke(DiffereImportMessage $directImportMessage)
    {
        $download = $this->entityManager->find(Download::class, $directImportMessage->getDownloadId());

        if( $download !== null){
            return $this->queueService->action($download);
        }

    }
}