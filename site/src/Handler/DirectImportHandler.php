<?php

declare(strict_types=1);

namespace App\Handler;

use App\Entity\Download;
use App\Message\DirectImportMessage;
use App\Service\ImportQueueService;
use Doctrine\ORM\EntityManagerInterface;
use Memcached;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;


#[AsMessageHandler(fromTransport: 'async_priority_high', priority: 10)]
class DirectImportHandler
{
    private EntityManagerInterface $entityManager;
    private ImportQueueService $queueService;


    public function __construct(EntityManagerInterface $entityManager, ImportQueueService $queueService)
    {
        $this->entityManager = $entityManager;
        $this->queueService = $queueService;
    }

    public function __invoke(DirectImportMessage $directImportMessage)
    {
        $download = $this->entityManager->find(Download::class, $directImportMessage->getDownloadId());

        if( $download !== null){
            return $this->queueService->action($download);
        }

    }

}