<?php

declare(strict_types=1);

namespace App\Service;

use AllowDynamicProperties;
use App\Entity\Download;
use App\Entity\Status;
use App\Exception\BadRequestException;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use JetBrains\PhpStorm\NoReturn;
use Prodige\ProdigeBundle\Services\AdminClientService;
use Prodige\ProdigeBundle\Services\CurlUtilities;
use Prodige\ProdigeBundle\Services\UserService;
use Psr\Log\LoggerInterface;
use Doctrine\Persistence\ManagerRegistry;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Process\Process;


#[AllowDynamicProperties] class ImportQueueService
{
    private string $GDAL_TRANSLATE_CMD = "%GDAL_TRANSLATE_PATH% --config GDAL_CACHEMAX 300 -projwin %GDAL_PROJ_WIN% -of %FORMAT_OPTION% %FILE_IN% %FILE_OUT%";
    private Connection $catalogueConnect;
    private Connection $prodigeConnect;
    private string $host;
    private string $port;
    private string $user;
    private string $dbname;
    private string $password;
    private string $espgGeojson;
    private string $espgProImport;
    private string $ogrCmd;
    private string $schema;
    private string $directory;
    private string $gdalWarpPath;
    private string $rasterDirectory;


    public function __construct(
        private ParameterBagInterface $parameterBag,
        private RightsService $rightsService,
        private LoggerInterface $logger,
        private EntityManagerInterface $entityManager,
        private ManagerRegistry $doctrine,
        private MailerService $mailerService,
        private MailerInterface $mailer,
        private UserService $userService,
        private FileService $fileService,
        private AdminClientService $adminClientService,
        private MetadataService $metadataSheetService
    ) {
        $this->catalogueConnect = $doctrine->getConnection('catalogue');
        $this->prodigeConnect = $doctrine->getConnection('prodige');
        $this->host = $parameterBag->get('prodige_host');
        $this->port = $parameterBag->get('prodige_port');
        $this->user = $parameterBag->get('prodige_user');
        $this->dbname = $parameterBag->get('prodige_name');
        $this->password = $parameterBag->get('prodige_password');
        $this->espgGeojson = $parameterBag->get('IMPORT_DEFAULT_GEOJSON_ESPG');
        $this->espgProImport = $parameterBag->get('PRODIGE_DEFAULT_EPSG');
        $this->directory = $this->parameterBag->get('TELECHARGEMENT_DOWNLOAD_PATH');
        $this->ogrCmd = $parameterBag->get('CMD_OGR2OGR');
        $this->schema = "public";
        $this->gdalWarpPath = $this->parameterBag->get('GDAL_WARP_PATH');
        $this->translateGdalPath = $this->parameterBag->get('GDAL_TRANSLATE_PATH');
        $this->rasterDirectory = $this->parameterBag->get('RASTER_PATH');
    }

    /**
     * @param Download $download
     */
    public function action(Download $download): void
    {
        $rigths = $this->rightsService->getRightAction($download->getMetadataUuid());

        if (is_array($rigths) && array_key_exists('error', $rigths)) {
            $this->notifyAndStopDownload($download, $rigths['error']);
            return;
        } elseif (is_bool($rigths)) {
            $this->notifyAndStopDownload($download, 'Droit non récupéré');
            return;
        }
        if ($rigths['canDownloadData']) {
            // crée le répertoire principal de la demande de téléchargement
            $strMainTeleDir = "Telechargement_" . time() . "_" . mt_rand(1000, 9999);

            if (!mkdir(
                    $concurrentDirectory = $this->directory . $strMainTeleDir,
                    0777,
                    true
                ) && !is_dir($concurrentDirectory)) {
                $this->notifyAndStopDownload(
                    $download,
                    sprintf('Directory "%s" was not created', $concurrentDirectory)
                );
                return;
            }

            $metadataInfo = $this->getMetadataInfo($rigths['publicId']);
            $strDirectory = 'metadata';
            $toDirectory = $this->directory . $strMainTeleDir . "/" . $strDirectory;

            $dataDirectory = $this->directory . $strMainTeleDir . "/data/";
            //[1 = vecteur, 2 = vue, 3 = tabulaire, 4 = rasteur]
            switch ($rigths['data']['couche_type']) {
                case 1 :
                    $action = $this->treat_data_vector(
                        $download,
                        $dataDirectory,
                        $rigths['publicId']
                    );
                    break;
                case 3 :
                    $action = $this->treat_data_table($dataDirectory, $download, $rigths['publicId']);
                    break;
                case 2 :
                    $action = $this->treat_data_raster(
                        $dataDirectory,
                        $download,
                        $rigths
                    );
                    break;
            }

            if (is_array($action) && array_key_exists('error', $action)) {
                $this->notifyAndStopDownload(
                    $download,
                    mb_convert_encoding($action['error'], 'UTF-8', 'ISO-8859-1'),
                    $strMainTeleDir
                );
                return;
            }


            $createXmlPdf = $this->fileService->insertXml($metadataInfo, $toDirectory, $download->getMetadataUuid());

            if ($createXmlPdf === false) {
                $this->notifyAndStopDownload(
                    $download,
                    mb_convert_encoding('Download failed', 'UTF-8', 'ISO-8859-1'),
                    $strMainTeleDir
                );
                return;
            }

            $getZipMetadata = $this->fileService->insertFullPDF(
                $this->directory . $strMainTeleDir . "/metadata/",
                $download->getMetadataUuid()
            );

            if ($getZipMetadata === false) {
                $this->notifyAndStopDownload(
                    $download,
                    mb_convert_encoding('Download failed', 'UTF-8', 'ISO-8859-1'),
                    $strMainTeleDir
                );
                return;
            }
            $compressDirectory = $this->fileService->actionZipDirectory(
                $this->directory,
                $strMainTeleDir
            );

            if (array_key_exists('error', $compressDirectory)) {
                $this->notifyAndStopDownload($download, $compressDirectory['error']);
                return;
            }

            if (isset($action)) {
                $response = $this->parameterBag->get(
                        'PRODIGE_URL_TELECHARGEMENT'
                    ) . "/api/download/" . $download->getId() . "/file";
                $status = $this->entityManager->getRepository(Status::class)->findOneBy(['id' => 3]);
                $download->setStatus($status);
                $download->setFile($compressDirectory['realPath']);
                $download->setResponse($response);
                $download->setQueueName(null);
                $this->entityManager->persist($download);
                $this->entityManager->flush();
                $this->fileService->deleteDirectory(
                    $this->directory . $strMainTeleDir
                );
                if (!$download->isDirect()) {
                    $this->mailerService->sendMail($this->mailer, $download);
                }
            }
        } else {
            $this->notifyAndStopDownload(
                $download,
                "ERREUR : vos droits ne vous permettent pas d'accéder à la donnée " . $download->getMetadataUuid()
            );
        }
    }

    #[NoReturn] private function notifyAndStopDownload(
        Download $download,
        string $errorMessage,
        string $removeDir = null
    ) {
        $failedStatus = $this->entityManager->getRepository(Status::class)->findOneBy(['id' => 4]);

        $download->setStatus($failedStatus);
        $download->setResponse(
            $errorMessage . $download->getMetadataUuid()
        );
        switch ($download->getQueueName()) {
            case 'direct':
                $download->setQueueName("direct ( failed )");
                break;
            case 'differed':
                $download->setQueueName("differed ( failed )");
                break;
            case 'failed_low_priority':
                $download->setQueueName("differed ( undeliverable )");
                break;
            case 'failed_high_priority':
                $download->setQueueName("direct ( undeliverable )");
                break;
        }

        $this->entityManager->persist($download);
        $this->entityManager->flush();
        if (!$download->isDirect()) {
            $this->mailerService->sendMail($this->mailer, $download);
        }

        if ($removeDir !== null) {
            $this->fileService->removeDir($removeDir);
        }
    }

    /**
     * @param int $metadataId
     * @return array
     */
    public function getMetadataInfo(int $metadataId): array
    {
        $strSql = /*"set search_path to public;" .*/
            " SELECT m.id, m.uuid, m.data FROM public.metadata as m" .
            " WHERE m.id=?";

        //$dao = new DAO();
        $conn = $this->catalogueConnect;
        $result = array();
        $tabMetadataInfo = $conn->fetchAllAssociative($strSql, array($metadataId));

        if (!empty($tabMetadataInfo)) {
            $result['success'] = true;
            $result['metadata_id'] = $tabMetadataInfo[0]["id"];
            $result['metadata_uuid'] = ($tabMetadataInfo[0]["uuid"]);

            //On ne remplace pas les caracteres html contenus dans le xml (probleme de decodage du xml sinon notamment avec les chevrons < et >)
            $result['metadata_data'] = ($tabMetadataInfo[0]["data"]);
        }

        return $result;
    }

    /**
     * @param Download $download
     * @param string $toDirectory
     * @param int $publicId
     * @return array
     */
    public function treat_data_vector(
        Download $download,
        string $toDirectory,
        int $publicId
    ): array {
        $metadata = $this->getMetadataSheetAdminInfo($publicId);
        $adminInfo = $this->metadataSheetService->getMetadataInfo($publicId);
        if (array_key_exists('error', $metadata)) {
            return ['error' => $metadata['error'], 'status' => 404];
        }
        $layers = $adminInfo['layers'];

        if (count($layers) > 1) {
            $isMulti = true;
            $countError = 0;
        } else {
            $isMulti = false;
        }

        foreach ($layers as $layerInfo) {
            $fileName = $layerInfo['storagePath'];

            if (!mkdir($inDirectory = $toDirectory . $fileName . "/", 0777, true) && !is_dir($inDirectory)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $inDirectory));
            }

            $originalExtension = "Postgis";
            $default_extension = "the_current_format";
            $tabFormats = array(
                "shp" => array("format" => "ESRI Shapefile", "nln" => false, "extension" => $default_extension),
                "tab" => array("format" => "MapInfo File", "nln" => false, "extension" => $default_extension),
                "mif" => array("format" => "MapInfo File", "nln" => false, "extension" => $default_extension),
                "kml" => array("format" => "KML", "nln" => false, "extension" => $default_extension),
                "gml" => array("format" => "GML", "nln" => false, "extension" => $default_extension),
                "csv" => array("format" => "CSV", "nln" => false, "extension" => $default_extension),
                "bna" => array("format" => "BNA", "nln" => false, "extension" => $default_extension),
                "gxt" => array("format" => "Geoconcept", "nln" => false, "extension" => $default_extension),
                "gmt" => array("format" => "GMT", "nln" => false, "extension" => $default_extension),
                "dgn" => array("format" => "DGN", "nln" => false, "extension" => $default_extension),
                "dxf" => array("format" => "DXF", "nln" => false, "extension" => $default_extension),
                "json" => array("format" => "GeoJSON", "nln" => false, "extension" => $default_extension),
                "xyz" => array(
                    "format" => "CSV",
                    "nln" => true,
                    "extension" => $default_extension,
                    "lco" => array("SEPARATOR=TAB")
                ),
                "asc" => array(
                    "format" => "CSV",
                    "nln" => true,
                    "extension" => $default_extension,
                    "lco" => array("SEPARATOR=COMMA")
                ),
                "sql" => array("format" => "PGDump", "nln" => true, "extension" => $default_extension),
            );
            $bClip = false;
            $formatProperties = $tabFormats[strtolower($download->getFormat())];
            if ($formatProperties["extension"] === $default_extension) {
                $formatProperties["extension"] = strtolower($download->getFormat());
            }
            $strFileName = $fileName . "." . $formatProperties["extension"];

            $strCommande = implode(" ", array(
                "mkdir -p " . escapeshellarg($inDirectory) . ";",

                $this->parameterBag->get('CMD_OGR2OGR'),
                '-gt 10000',
                '-f "' . $formatProperties["format"] . '"',
                ($formatProperties["nln"] ? '-nln "' . ($formatProperties["extension"] === "sql" ? $fileName : $strFileName) . '"' : ''),
                '"' . $inDirectory . $strFileName . '"',
                ''
            ));
            //check prj ressources
            //TODO Voir avec __dir__
            if ($download->getSrs()) {
                if (file_exists(
                    realpath(dirname(__FILE__)) . "/ressources/" . str_replace(
                        ":",
                        "",
                        $download->getSrs()
                    ) . "_" . $download->getFormat() . ".srs"
                )) {
                    $strCommande .= " -t_srs " . $download->getSrs() . " -a_srs " . realpath(
                            dirname(__FILE__)
                        ) . "/ressources/" . str_replace(":", "", $download->getSrs()) . "_" . $download->getFormat(
                        ) . ".srs ";
                } else {
                    if (file_exists(
                        realpath(dirname(__FILE__)) . "/ressources/" . str_replace(
                            ":",
                            "",
                            $download->getSrs()
                        ) . ".srs"
                    )) {
                        $strCommande .= " -t_srs " . $download->getSrs() . " -a_srs " . realpath(
                                dirname(__FILE__)
                            ) . "/ressources/" . str_replace(":", "", $download->getSrs()) . ".srs ";
                    } else {
                        $strCommande .= " -t_srs " . $download->getSrs() . " -a_srs " . $download->getSrs() . " ";
                    }
                }
            }

            $tabInfoTables = explode(".", $fileName);
            $schemaName = (isset($tabInfoTables[1]) ? $tabInfoTables[0] : "public");
            $tableName = ($tabInfoTables[1] ?? $tabInfoTables[0]);
            $strSql = "select 1 from information_schema.tables where table_schema=:table_schema and table_name=:table_name ";
            try {
                $exists = $this->prodigeConnect->fetchOne(
                    $strSql,
                    ['table_schema' => $schemaName, 'table_name' => $tableName]
                );
            } catch (Exception $e) {
                return ['error' => $e, 'status' => 404];
            }

            if (!$exists) {
                return ['error' => "ERREUR : La table " . $tableName . " n'existe pas", 'status' => 404];
            }

            $parametersConn = "PG:\"host=" . $this->host .
                " user=" . $this->user . " dbname=" . $this->dbname . " password=" . $this->password . "\"";
            //Postgis
            $strCommande .= $parametersConn;

            $input_srid = "";
            $input_type = "";

            // récupère les propriétés de la table et les affecte à la sortie
            $strSql = "select * from public.geometry_columns where f_table_schema=? and f_table_name=?";

            $tabTable = $this->prodigeConnect->fetchAllAssociative($strSql, array($schemaName, $tableName));

            if (!empty($tabTable)) {
                $input_srid = $tabTable[0]["srid"];
                $input_type = $tabTable[0]["type"];
            }

            if ($input_srid === 0) {
                $tableExists = $this->prodigeConnect->fetchAll(
                    "SELECT to_regclass('" . $schemaName . "." . $tableName . "') as exists"
                );
                if (!empty($tableExists) && $tableExists[0]["exists"] !== $tableName) {
                    return ['error' => "ERREUR : La table " . $tableName . " n'existe pas", 'status' => 404];
                }
            }

            $geomExists = $this->prodigeConnect->fetchAllAssociative(
                "SELECT 1 FROM   pg_attribute WHERE  attrelid = '" . $tableName . "'::regclass  -- cast to a registered class (table)
                                                      AND    attname = 'the_geom'
                                                      AND    NOT attisdropped "
            );

            if (!empty($geomExists)) {
                $geomInfo = $this->prodigeConnect->fetchAllAssociative(
                    "select public.st_srid(the_geom) as srid, public.geometrytype(the_geom) as type
                                                    from " . $schemaName . "." . $tableName . " limit 1"
                );
                if (!empty($geomInfo)) {
                    $input_srid = $geomInfo[0]["srid"];
                    $input_type = $geomInfo[0]["type"];
                }
            } else {
                return [
                    'error' => sprintf(
                        "[%s] Erreur lors de la génération: , la table ne contient pas de géométries\n",
                        date("Y-m-d G:i:s")
                    ),
                    'status' => 404
                ];
            }

            if (!empty($download->getExtract())) {
                if (array_key_exists('buffer', $download->getExtract())) {
                    $buffer = $download->getExtract()['buffer'];
                } else {
                    $buffer = "";
                }
                switch ($download->getExtract()['type']) {
                    case "polygon":
                        $wkt = json_encode($download->getExtract()['area']);

                        $strSql = $this->createSqlClipFromWkt(
                            $tableName,
                            $schemaName,
                            $buffer,
                            "st_intersects",
                            $wkt,
                            $input_srid
                        );
                        break;
                    case "list":
                        $tabTerritoire = $this->getTerritoireInfo($download->getExtract()['area_type_id']);

                        if (empty($tabTerritoire) || (is_array($tabTerritoire) && array_key_exists(
                                    'error',
                                    $tabTerritoire
                                ))) {
                            return ['error' => "Les territoires n'ont pas pu être récuperés", 'status' => 404];
                        }

                        $tableTerritoireName = $tabTerritoire['data']['layer']['storagePath'];
                        $champTerritoireIdName = $tabTerritoire['data']['fieldId'];
                        //TODO: controler la présence de toutes les données avant de lancer
                        $strSql = $this->createSqlClip(
                            $tableName,
                            $buffer,
                            "st_intersects",
                            $tableTerritoireName,
                            $champTerritoireIdName,
                            $download->getExtract()['area_id'],
                            true,
                            $input_type
                        );
                        break;
                }

                if ($strSql) {
                    $strCommande .= " -sql \"" . $strSql . "\"";
                    $bClip = true;
                }
            }

            // ajoute la requête de clippage
            if (!$bClip) {
                $tabColumnsInfo = $this->getTableColumns($this->prodigeConnect, $tableName);
                $strTableFields = "";
                foreach ($tabColumnsInfo as $column) {
                    if ($column['field'] !== 'gid') {
                        $strTableFields .= '"' . $column['field'] . '"' . ',';
                    }
                }
                $strTableFields = rtrim($strTableFields, ',');

                if (strtolower($download->getFormat()) === "asc" || strtolower($download->getFormat()) === "xyz") {
                    $strOrder = " order by y,x asc ";
                } else {
                    $strOrder = "";
                }
                $strCommande .= " -sql \"select distinct " . ($strTableFields ?: "*") . " from " . $schemaName
                    . "." . $tableName . $strOrder . " \"";
            }

            if ($input_srid !== "-1") {
                $strCommande .= " -s_srs EPSG:" . $input_srid . " -nlt \"PROMOTE_TO_MULTI\"";
            }

            if (!in_array($download->getFormat(), ["json", "tab", "mif", "gml", "kml", "dgn", "dxf"])) {
                $strCommande .= " -lco ENCODING=UTF-8";
            }

            $process = Process::fromShellCommandline($strCommande . " 2>&1");
            $process->setTimeout(null);
            $process->run();

            if (preg_match('/option ENCODING/', $process->getOutput())) {
                $strCommande = str_replace('UTF-8', 'LATIN-1', $strCommande);
                $process = Process::fromShellCommandline($strCommande . " 2>&1");
                $process->setTimeout(null);
                $process->run();
            }

            if (!in_array($download->getFormat(), ["gxt", "kml", "dgn", "dxf"])) {
                if (preg_match('/Warning 6: Normalized/', $process->getOutput()) < 1) {
                    if ($isMulti === false && $process->getOutput() !== "") {
                        return ['error' => $process->getOutput(), 'status' => 404];
                    }
                }
            }

            if ($isMulti === true && $process->getOutput() !== "") {
                +$countError;
            }
        }
        if ($isMulti === true && (isset($countError) && count($layers) === $countError)) {
            return ['error' => $process->getOutput(), 'status' => 404];
        }

        return ['data' => $toDirectory, 'status' => 201];
    }

    /**
     * @param int $publicId
     * @return array
     */
    public function getMetadataSheetAdminInfo(
        int $publicId
    ): array {
        $this->logger->info('Get metadata_sheets info from admin');
        $url = $this->parameterBag->get(
                'PRODIGE_URL_ADMIN'
            ) . '/api/metadata_sheets?page=1&itemsPage=30&publicMetadataId=' . $publicId;
        $response = $this->adminClientService->curlAdminWithAdmincli($url, 'GET');

        if ($response["hydra:totalItems"] === 0) {
            $this->logger->error('curl metadata_sheets info error ', ['error' => $response]);
            return ["error" => 'metadata_sheet not found', "status" => 404];
        }
        if (array_key_exists("hydra:member", $response)) {
            return ["data" => $response['hydra:member'][0], "status" => 200];
        }
        return ["error" => 'echec de la demande', "status" => 409];
    }

    /**
     * @param string $tableDataName
     * @param string $schemaName
     * @param string|int $buffer
     * @param string $strFonctionName
     * @param string $wktGeom
     * @param int $input_type
     * @return string
     * @throws Exception
     */
    public function createSqlClipFromWkt(
        string $tableDataName = "",
        string $schemaName = "public",
        string|int $buffer = 0,
        string $strFonctionName = "st_intersects",
        string $wktGeom = "",
        int $input_type = -1
    ): string {
        $strSql = "";

        // sort si un ou plusieurs paramètres sont manquants ou erronés
        if (!$tableDataName || ($strFonctionName !== "st_intersects" && $strFonctionName !== "st_within") || $wktGeom === "") {
            return $strSql;
        }
        // récupère les champs de la table des données
        $tabColumnsInfo = $this->getTableColumns($this->prodigeConnect, $tableDataName);

        if (empty($tabColumnsInfo)) {
            return $strSql;
        }
        $strTableFields = "";
        for ($i = 0; $i < sizeof($tabColumnsInfo); $i++) {
            if ($tabColumnsInfo[$i]["field"] !== "the_geom" && $tabColumnsInfo[$i]["field"] !== "gid") {
                $strTableFields .= $schemaName . "." . $tableDataName . ".\"" . ($tabColumnsInfo[$i]["field"]) . "\", ";
            }
        }

        // crée la requête de sélection
        $geomMask = "st_buffer(ST_Transform(ST_SetSRID(ST_GeomFromGeoJSON('" . $wktGeom . "'), 4326),"
            . $input_type . ")," . $buffer . ")";

        $strSql .= " SELECT * from( ";
        $strSql .= " SELECT " . $strTableFields . "st_multi(st_intersection(" .
            //cas POLYGON, nettoyage des géométries par buffer 0 avant découpage
            ($input_type === "MULTIPOLYGON"
                ? "st_buffer(" . $schemaName . "." . $tableDataName . ".the_geom,0)"
                : $schemaName . "." . $tableDataName . ".the_geom") .
            ", " . $geomMask . ")) as intersection";

        $strSql .= " FROM " . $schemaName . "." . $tableDataName;
        $strSql .= " WHERE " . $schemaName . "." . $tableDataName . ".the_geom && st_expand(st_transform(st_setsrid(ST_GeomFromGeoJSON('" . $wktGeom . "'),4326)," . $input_type . ")," . $buffer . ")" .
            " AND st_isvalid(st_buffer(" . $schemaName . "." . $tableDataName . ".the_geom,0) )= true " .
            " AND " . $strFonctionName . "(" . $schemaName . "." . $tableDataName . ".the_geom, " . $geomMask . ")";
        $strSql .= " ) clip  where st_geometrytype(clip.intersection)<> 'GEOMETRYCOLLECTION'";

        return str_replace("\"", "\\\"", $strSql);
    }

    /**
     * @param Connection $conn
     * @param string $tableDataName
     * @return array[]
     * @throws Exception
     */
    public function getTableColumns(
        Connection $conn,
        string $tableDataName
    ): array {
        $strSql_getTableColumns = "SELECT a.attnum as NUMB, a.attname as FIELD, t.typname as TYPE, a.attlen as LEN, a.atttypmod, a.attnotnull, a.atthasdef as DEF" .
            " FROM pg_class c" .
            "  inner join pg_attribute a on a.attrelid = c.oid" .
            "  inner join pg_namespace n ON n.oid = c.relnamespace" .
            "  inner join pg_type t on a.atttypid = t.oid" .
            " WHERE c.relname = '" . mb_strtolower($tableDataName) . "'" .
            " AND n.nspname = '" . mb_strtolower($this->schema) . "'" .
            " and a.attnum > 0" .
            " ORDER BY attnum";

        return $conn->fetchAllAssociative($strSql_getTableColumns);
    }

    /**
     * @param int $publicId
     * @return array
     */
    public function getTerritoireInfo(
        int $territoireId
    ): array {
        $this->logger->info('Get territoire info from admin');
        $url = $this->parameterBag->get(
                'PRODIGE_URL_ADMIN'
            ) . '/api/engines/' . $territoireId;
        $curl = CurlUtilities::curl_init($url);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/ld+json'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
        $response = curl_exec($curl);
        $jsonResponse = json_decode($response, true);
        curl_close($curl);

        if ($jsonResponse === null) {
            $this->logger->error('curl territoire info error ', ['error' => $response]);
            return ["error" => $response, "status" => 409];
        }
        if ($jsonResponse["@type"] === "hydra:Error") {
            return ["error" => $jsonResponse["hydra:description"], "status" => 404];
        }
        if (array_key_exists("id", $jsonResponse)) {
            unset($jsonResponse['@context'], $jsonResponse['@id'], $jsonResponse['@type'], $jsonResponse['rowOrder']);
            return ["data" => $jsonResponse, "status" => 200];
        }
        return ["error" => 'echec de la demande', "status" => 409];
    }

    /**
     * @param string $tableDataName
     * @param int|string $buffer
     * @param string $strFonctionName
     * @param string $tableTerritoireName
     * @param string $champTerritoireIdName
     * @param array $tabTerritoireId
     * @param bool $bIntersectGeom
     * @param string $input_type
     * @return string
     * @throws Exception
     */
    public function createSqlClip(
        string $tableDataName = "",
        int|string $buffer = 0,
        string $strFonctionName = "st_intersects",
        string $tableTerritoireName = "",
        string $champTerritoireIdName = "",
        array $tabTerritoireId = [],
        bool $bIntersectGeom = true,
        string $input_type = ""
    ): string {
        $strSql = "";
        if ($buffer === "") { //pour les extractions liées aux restrictions territoriales, buffer = ""
            $buffer = 1;
        }
        // sort si un ou plusieurs paramètres sont manquants ou erronés
        if ($tableDataName === "" || ($strFonctionName !== "st_intersects" && $strFonctionName !== "st_within") ||
            $tableTerritoireName === "" || $champTerritoireIdName === "" || empty($tabTerritoireId)) {
            return $strSql;
        }

        // crée une chaîne qui contient les identifiants des territoires séparés par une virgule
        $strTerritoireId = "'" . implode("', '", $tabTerritoireId) . "'";

        // récupère les champs de la table des données
        $tabColumnsInfo = $this->getTableColumns($this->prodigeConnect, $tableDataName, $this->schema);

        //TODO decect one geom column, not put the_geom
        if (empty($tabColumnsInfo)) {
            return $strSql;
        }

        // crée la requête de sélection
        $strSql .= " select * from( ";

        if ($bIntersectGeom) {
            $strTableFields = "";
            for ($i = 0; $i < sizeof($tabColumnsInfo); $i++) {
                if ($tabColumnsInfo[$i]["field"] !== "the_geom" && $tabColumnsInfo[$i]["field"] !== "gid") {
                    $strTableFields .= $this->schema . "." . $tableDataName . ".\\\"" ./*utf8_encode*/
                        ($tabColumnsInfo[$i]["field"]) . "\\\", ";
                }
            }
            $strSql .= " SELECT " . $strTableFields . "st_multi(st_intersection(st_transform(" .
                //cas POLYGON, nettoyage des géométries par buffer 0 avant découpage
                ($input_type === "MULTIPOLYGON"
                    ? "st_buffer(" . $this->schema . "." . $tableDataName . ".the_geom,0)"
                    : $this->schema . "." . $tableDataName . ".the_geom") .
                ", " . $this->espgProImport . ")" .
                ", (SELECT st_buffer(st_union(the_geom), " . $buffer . ") FROM public." . $tableTerritoireName .
                " WHERE " . $champTerritoireIdName . " IN (" . $strTerritoireId . ")))) as intersection";
        } else {
            $strTableFields = "";
            for ($i = 0; $i < sizeof($tabColumnsInfo); $i++) {
                if ($tabColumnsInfo[$i]["field"] !== "gid") {
                    $strTableFields .= $this->schema . "." . $tableDataName . ".\\\"" ./*utf8_encode*/
                        ($tabColumnsInfo[$i]["field"]) . "\\\", ";
                }
            }
            $strTableFields = substr($strTableFields, 0, -2);
            $strSql .= " SELECT " . $strTableFields;
        }
        $strSql .= " FROM " . $this->schema . "." . $tableDataName;

        $strSql .= " WHERE  st_transform(" . $this->schema . "." . $tableDataName . ".the_geom, " . $this->espgGeojson .
            ") && (st_expand((SELECT st_union(the_geom) FROM public." . $tableTerritoireName . " WHERE " . $champTerritoireIdName .
            " IN (" . $strTerritoireId . ")), " . $buffer . "))" .
            " AND st_isvalid(" . $this->schema . "." . $tableDataName . ".the_geom )= true " .
            " AND " .
            ($input_type === "MULTIPOLYGON" ?
                $strFonctionName . "(st_buffer(st_transform(" . $this->schema . "." . $tableDataName . ".the_geom, " . $this->espgGeojson . "), 0)" :
                $strFonctionName . "(st_transform(" . $this->schema . "." . $tableDataName . ".the_geom, " . $this->espgGeojson . ")"
            ) . ", (SELECT st_buffer(st_union(the_geom), " . $buffer . ") FROM public." . $tableTerritoireName .
            " WHERE " . $champTerritoireIdName . " IN (" . $strTerritoireId . ")))";

        $strSql .= " ) clip  ";
        if ($bIntersectGeom) {
            $strSql .= "where st_geometrytype(clip.intersection)<> 'GEOMETRYCOLLECTION'";
        }
        return $strSql;
    }

    /**
     * @param string $strMainTeleDir
     * @param Download $download
     * @param int $publicId
     * @return array
     * @throws Exception
     */
    public function treat_data_table(string $toDirectory, Download $download, int $publicId): array
    {
        $metadataInfo = $this->getMetadataSheetAdminInfo($publicId);

        if (array_key_exists('error', $metadataInfo)) {
            return ['error' => $metadataInfo['error'], 'status' => 404];
        }
        $layers = [];
        foreach ($metadataInfo['data']['layers'] as $layerAdmin) {
            $layers[] = [
                'storagePath' => $layerAdmin['storagePath'],
                'name' => $layerAdmin['name']
            ];
        }

        if (count($layers) > 1) {
            $isMulti = true;
            $countError = 0;
        } else {
            $isMulti = false;
        }

        foreach ($layers as $layerInfo) {
            $fileName = $layerInfo['storagePath'];
            $strCommande = $this->ogrCmd . ' -lco ENCODING="UTF-8" ';
            if (!mkdir($inDirectory = $toDirectory . $fileName . "/", 0777, true) && !is_dir($inDirectory)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $inDirectory));
            }


            switch (strtolower($download->getFormat())) {
                case "csv":
                    $strFileName = $fileName . ".csv";
                    $strCommande .= "-f \"CSV\" " . $inDirectory . $strFileName;
                    break;
                case "ods":
                    $strFileName = $fileName . ".ods ";
                    $strCommande .= "-f \"ODS\" " . $inDirectory . $strFileName;
                    break;
                case "xlsx":
                    $strFileName = $fileName . ".xlsx ";
                    $strCommande .= "-f \"XLSX\" " . $inDirectory . $strFileName;
                    break;
                case "sql":
                    $strFileName = $fileName . ".sql ";
                    $strCommande .= "-f \"PGDump\" " . $inDirectory . $strFileName;
                    break;
            }

            $strCommande .= " PG:\"host=" . $this->host .
                " user=" . $this->user . " dbname=" . $this->dbname . " password=" . $this->password . "\"";

            $tabColumnsInfo = $this->getTableColumns($this->prodigeConnect, $fileName);
            $strTableFields = "";
            $glue = "";
            for ($i = 0; $i < sizeof($tabColumnsInfo); $i++) {
                if ($tabColumnsInfo[$i]["field"] !== "gid") {
                    $strTableFields .= $glue . $this->schema . "." . $fileName . ".\\\"" . ($tabColumnsInfo[$i]["field"]) . "\\\"";
                    $glue = ",";
                }
            }
            $strCommande .= " -sql \"select distinct " . ($strTableFields ?: "*") . " from " . $this->schema .
                ".\"" . $fileName . "\" \"";

            $process = Process::fromShellCommandline($strCommande . " 2>&1");
            $process->setTimeout(10800);
            $process->run();

            if (preg_match('/option ENCODING/', $process->getOutput())) {
                $strCommande = str_replace('UTF-8', 'LATIN-1', $strCommande);
                $process = Process::fromShellCommandline($strCommande . " 2>&1");
                $process->setTimeout(null);
                $process->run();
            }

            if ($isMulti === true && $process->getOutput() !== "") {
                +$countError;
            }
        }
        if ($isMulti === true && (isset($countError) && count($metadataInfo['data']['layers']) === $countError)) {
            return ['error' => $process->getOutput(), 'status' => 404];
        }

        return ['data' => $toDirectory, 'status' => 201];
    }

    private function treat_data_raster(
        string $dataDirectory,
        Download $download,
        array $rigths
    ) {
        $adminInfo = $this->metadataSheetService->getMetadataInfo($rigths['publicId']);
        if (array_key_exists('error', $adminInfo)) {
            return ['error' => $adminInfo['error'], 'status' => 404];
        }
        $rastersInfo = [];
        foreach ($adminInfo['layers'] as $layerInfo) {
            $response = $this->getRasterInfo($layerInfo['id']);
            if (array_key_exists('error', $response)) {
                return ['error' => $response['error'], 'status' => 404];
            }

            $rastersInfo[] = $response[0];
        }

        foreach ($rastersInfo as $rasterInfo) {
            if (isset($rasterInfo['vrt_path'])) {
                $fileName = basename($rasterInfo['vrt_path'], ".vrt") . "_extract";
                $strDirectory = $fileName . "_" . time() . "_" . mt_rand(1000, 9999);
                $path = $dataDirectory . $strDirectory . "/";

                @mkdir($path, 0777, true);
                $fileName1 = basename($rasterInfo['vrt_path'], ".vrt") . "_reproj" . ".vrt";

                if (!empty($download->getExtract())) {
                    if ($download->getExtract()['buffer'] > 100 && $download->getExtract()['buffer'] < 5000) {
                        $buffer = $download->getExtract()['buffer'];
                    } else {
                        if ($download->getExtract()['buffer'] > 100) {
                            $buffer = 100;
                        } elseif ($download->getExtract()['buffer'] < 5000) {
                            $buffer = 500;
                        }
                    }
                }

                $str2 = "SELECT st_xmin(T.the_geom) as xmin, st_ymin(T.the_geom) as ymin, st_xmax(T.the_geom) as xmax, st_ymax(T.the_geom) as ymax " . "FROM (SELECT box2d(st_transform(st_geometryfromtext(" .
                    "'POLYGON (( '|| :lower_left  ||', " . "  '|| :upper_left  ||', " . "  '|| :upper_right ||', " . "  '|| :lower_right ||', " .
                    "  '|| :lower_left  ||'))', :import_srid), :export_srid::integer)) as the_geom) T";

                $tabExtentReproj = $this->prodigeConnect->fetchAllAssociative(
                    $str2,
                    array(
                        "lower_left" => $rasterInfo["ulx"] . " " . $rasterInfo["lry"],
                        "upper_left" => $rasterInfo["lrx"] . " " . $rasterInfo["uly"],
                        "upper_right" => $rasterInfo["lrx"] . " " . $rasterInfo["uly"],
                        "lower_right" => $rasterInfo["ulx"] . " " . $rasterInfo["lry"],
                        "import_srid" => 2154,
                        "export_srid" => $rasterInfo["srid"]
                    )
                );

                if (empty($tabExtentReproj)) {
                    // écrit dans le fichier de log administrateur
                    $this->notifyAndStopDownload($download, "L'extension de reprojection n'a pas pû être récupéré");
                } else {
                    $strCmd = $this->GDAL_TRANSLATE_CMD;
                    // Carefull uly =ymax and not ymin
                    $strCmd = str_replace(
                        "%GDAL_PROJ_WIN%",
                        ($tabExtentReproj[0]["xmin"] . " " . $tabExtentReproj[0]["ymax"] . " " . $tabExtentReproj[0]["xmax"] . " " . $tabExtentReproj[0]["ymin"]),
                        $strCmd
                    );
                    $strCmd = str_replace("%FILE_IN%", $rasterInfo['vrt_path'], $strCmd);
                    $strCmd = str_replace("%GDAL_TRANSLATE_PATH%", $this->translateGdalPath, $strCmd);
                    switch (strtolower($download->getFormat())) {
                        case "gtiff":
                            $strFileName = $fileName . ".tiff ";
                            $strWorldFileName = $fileName . ".tfw";
                            $formatOption = "GTIFF -co TFW=YES -co BIGTIFF=YES";
                            break;
                        case "ecw":
                            $strFileName = $fileName . ".ecw ";
                            //TODO hismail - PRO_LICENCE_ECW introuvable
                            $PRO_LICENCE_ECW = "off"; // Pour tester
                            $formatOption = "ECW" . ($PRO_LICENCE_ECW === "on" ? " -co LARGE_OK=YES" : "");
                            // when paletted color, force expansion to true color (3 bands) for ECW
                            if ($rasterInfo['color_interp'] === 0) {
                                $formatOption .= " -expand rgb";
                            }
                            break;
                        case "jpeg":
                        default:
                            $strFileName = $fileName . ".jpg ";
                            $strWorldFileName = $fileName . ".wld";
                            $formatOption = "JPEG -co WORLDFILE=YES";
                            if ($rasterInfo['color_interp'] === 0) {
                                $formatOption .= " -expand rgb";
                            }
                            break;
                    }

                    $strCmd = str_replace("%FORMAT_OPTION%", $formatOption, $strCmd);
                    $strCmd = str_replace("%FILE_OUT%", $path . $strFileName, $strCmd);

                    // 2 : creating raster extract file
                    $process = Process::fromShellCommandline($strCmd . " 2>&1");
                    $process->setTimeout(10800);
                    $process->run();

                    if ($process->isSuccessful() === false) {
                        return ['error' => $process->getOutput(), 'status' => 404];
                    }
                }
            }
        }
        return ['data' => $dataDirectory, 'status' => 201];
    }

    /**
     * @param int $id
     * @return array|mixed
     * @throws Exception
     */
    private function getRasterInfo(
        int $id
    ) {
        $conn = $this->catalogueConnect;
        $sql = "select * from catalogue.raster_info where id=:id";
        $rasterInfo = $conn->fetchAllAssociative($sql, ['id' => $id]);

        if ($rasterInfo !== false) {
            return $rasterInfo;
        }
        return ['error' => "les informations relatives au raster n'ont pas pû être récupérer", 'status' => 404];
    }

    /**
     * @param Download $download
     * @return bool
     */
    public function deleteInQueue(Download $download): bool
    {
        $conn = $this->catalogueConnect;

        if ($download->getQueueName() !== null) {
            $queueName = 'messenger_messages_' . $download->getQueueName() . '_import';
            $orderId = $download->getQueueId();
            $sql = "DELETE from telechargement." . $queueName . " where id=:id";

            $conn->fetchAssociative($sql, ['id' => $orderId]);
        }

        return true;
    }
}
