<?php

namespace App\Service;

use App\Entity\Download;
use App\Response\JsonLDResponse;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use FilesystemIterator;
use PharData;
use Prodige\ProdigeBundle\Services\AdminClientService;
use Prodige\ProdigeBundle\Services\CurlUtilities;
use Prodige\ProdigeBundle\Services\UserService;
use Psr\Log\LoggerInterface;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\FileinfoMimeTypeGuesser;
use Symfony\Component\Uid\Uuid;
use ZipArchive;

class FileService
{

    private $catalogueConnect;
    private Connection $prodigeConnect;
    private string $host;
    private string $port;
    private string $user;
    private string $dbname;
    private string $password;
    private string $espgGeojson;
    private string $espgProImport;
    private string $ogrCmd;
    private string $schema;
    private string $directory;
    private string $cliLogin;
    private string $cliPassword;


    public function __construct(
        private ParameterBagInterface $parameterBag,
        private LoggerInterface $logger,
        private ManagerRegistry $doctrine,
        private AdminClientService $adminClientService,
        private EntityManagerInterface $entityManager
    ) {
        $this->catalogueConnect = $doctrine->getConnection('catalogue');
        $this->prodigeConnect = $doctrine->getConnection('prodige');
        $this->host = $parameterBag->get('prodige_host');
        $this->port = $parameterBag->get('prodige_port');
        $this->user = $parameterBag->get('prodige_user');
        $this->dbname = $parameterBag->get('prodige_name');
        $this->password = $parameterBag->get('prodige_password');
        $this->espgGeojson = $parameterBag->get('IMPORT_DEFAULT_GEOJSON_ESPG');
        $this->espgProImport = $parameterBag->get('PRODIGE_DEFAULT_EPSG');
        $this->directory = $this->parameterBag->get('TELECHARGEMENT_DOWNLOAD_PATH');
        $this->ogrCmd = $parameterBag->get('CMD_OGR2OGR');
        $this->schema = "public";
        $this->cliLogin = $this->parameterBag->get('phpcli_default_login');
        $this->cliPassword = $this->parameterBag->get('phpcli_default_password');
    }

    /**
     * @param string $toDirectory
     * @param Uuid $uuid
     * @return bool
     */
    public function insertFullPDF(
        string $toDirectory,
        Uuid $uuid
    ): bool {
        $this->logger->info('Get Pdf Info info to catalogue');
        $url = $this->parameterBag->get('PRODIGE_URL_CATALOGUE') . "/metadata/pdf?uuid=" . $uuid;

        $curl = CurlUtilities::curl_init(
            $url,
            $this->cliLogin,
            $this->cliPassword
        );

        // Enregistrer le fichier
        $save = $toDirectory . "PDF_Complet_" . $uuid . ".zip";

        // Ouvrir le fichier
        $file = fopen($save, 'wb');

        // définit les option pour le transfert
        curl_setopt($curl, CURLOPT_FILE, $file);
        curl_setopt($curl, CURLOPT_HEADER, 0);

        $response = curl_exec($curl);

        curl_close($curl);

        fclose($file);
        if (!$response) {
            $this->logger->error('curl Pdf Info info to catalogue error ', ['error' => $response]);
            return false;
        }

        return true;
    }

    /**
     * @param array $metadataInfo
     * @param string $strMainTeleDir
     * @param Uuid $uuid
     * @return bool
     */
    public function insertXml(array $metadataInfo, string $strMainTeleDir, Uuid $uuid): bool
    {
        if (!mkdir(
                $currentDirectory = $strMainTeleDir,
                0777,
                true
            ) && !is_dir($currentDirectory)) {
            return false;
        }
        $action = file_put_contents(
            $strMainTeleDir . "/" . $uuid . ".xml",
            $metadataInfo['metadata_data']
        );

        $metadata_XMLData = ($metadataInfo['metadata_data']);
        $metadata_XMLData = preg_replace('/\s\s+/', ' ', $metadata_XMLData);
        $metadata_XMLData = preg_replace('/>\s+</', '><', $metadata_XMLData);
        $metadata_XMLData = str_replace(array("\r", "\n", "\t"), "", $metadata_XMLData);
        $metadata_XMLData = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>" . $metadata_XMLData;

        $dom = new \DomDocument();
        if ($dom->loadXML($metadata_XMLData)) {
            $identificationInfo = $dom->getElementsByTagName("identificationInfo")->item(0);
            if ($identificationInfo) {
                $MdIdentifier = $identificationInfo->getElementsByTagName("MD_Identifier")->item(0);
                if ($MdIdentifier) {
                    $CharacterString = $MdIdentifier->getElementsByTagName("CharacterString")->item(0);
                    if ($CharacterString) {
                        $linkSpecifFile = $CharacterString->nodeValue;

                        if ($linkSpecifFile) {
                            if (!str_starts_with($linkSpecifFile, "https")) {
                                $linkSpecifFile = $this->parameterBag->get(
                                        'PRO_GEONETWORK_URLBASE'
                                    ) . "/records/" . $uuid . "/formatters/xsl-view?output=pdf&approved=true";
                            }
                            $contentSpecifFile = @file_get_contents($linkSpecifFile);

                            if ($contentSpecifFile) {
                                if (($fname = $this->get_url_param_fname($linkSpecifFile)) !== "") {
                                    $specifFileName = $fname;
                                } elseif (($fname = substr(
                                        $linkSpecifFile,
                                        strrpos($linkSpecifFile, "/") + 1
                                    )) !== "") {
                                    $specifFileName = $fname;
                                } else {
                                    $specifFileName = "specification.txt";
                                }
                                if ($specifFile = fopen(
                                    $this->directory . "/" . $specifFileName,
                                    "w"
                                )) {
                                    fwrite($specifFile, $contentSpecifFile);
                                    fclose($specifFile);
                                }
                            }
                        }
                    }
                }
            }
        }

        return true;
    }

    /**********************************************************************
     * retourne la valeur du paramètre fname d'une url                    *
     * @param $url *
     * @return string   valeur du paramètre fname s'il existe, vide sinon *
     **********************************************************************/
    protected function get_url_param_fname($url): string
    {
        $res = "";
        $tabUrlParam = explode("?", $url);
        if (array_key_exists(1, $tabUrlParam)) {
            $strParam = $tabUrlParam[1];
            $tabParam = explode("&", $strParam);
            foreach ($tabParam as $param) {
                $tabParamNameValue = explode("=", $param);
                if ($tabParamNameValue[0] === "fname") {
                    $res = $tabParamNameValue[1];
                }
            }
        }
        return $res;
    }

    /**
     * @param string $rootSourceDirPath
     * @param string $sourceDirName
     * @return array
     */
    public function actionZipDirectory(
        string $rootSourceDirPath,
        string $sourceDirName
    ): array {
        if ($this->dirsize($rootSourceDirPath . $sourceDirName) > 4194304) { //4Go
            $strArchiveFile = $this->tarGzDirectory($rootSourceDirPath, $sourceDirName);
        } else {
            $strArchiveFile = $this->ZipDirectory($rootSourceDirPath, $sourceDirName);
        }

        return $strArchiveFile;
    }

    /**
     * @param string $mainDirectory
     * @return int
     */
    public function dirSize(string $mainDirectory): int
    {
        $Racine = opendir($mainDirectory);
        $Taille = 0;
        while ($Dossier = readdir($Racine)) {
            if ($Dossier !== '..' && $Dossier !== '.') {
                //Ajoute la taille du sous dossier
                if (is_dir($mainDirectory . '/' . $Dossier)) {
                    $Taille += $this->dirSize(
                        $mainDirectory . '/' .
                        $Dossier
                    );
                } //Ajoute la taille du fichier
                else {
                    $Taille += filesize($mainDirectory . '/' . $Dossier);
                }
            }
        }
        closedir($Racine);
        return $Taille;
    }

    /**
     * @param string $rootSourceDirPath
     * @param string $sourceDirName
     * @param PharData|null $pharData
     * @param string $extension
     * @return array
     */
    public function tarGzDirectory(
        string $rootSourceDirPath,
        string $sourceDirName,
        PharData $pharData = null,
        $extension = ".tar.gz"
    ): array {
        if ($pharData === null) {
            $pharData = new PharData($rootSourceDirPath . $sourceDirName . $extension);
        }

        $pharData->buildFromDirectory($rootSourceDirPath . $sourceDirName);

        return [
            'realPath' => $sourceDirName . $extension,
            "name" => $sourceDirName
        ];
    }

    /**
     * @param string $rootSourceDirPath
     * @param string $sourceDirName
     * @param string $rootZipPath
     * @param ZipArchive|null $zip
     * @param string $zipRelativePath
     * @param array $tabFilesIgnore
     * @return array
     */
    public function ZipDirectory(
        string $rootSourceDirPath,
        string $sourceDirName,
        ZipArchive $zip = null,
        string $zipRelativePath = "",
        array $tabFilesIgnore = [],
        $extension = ".zip"
    ): array {
        if ($zip === null) {
            $zip = new ZipArchive();
        }

        if ($zipRelativePath === "") {
            if ($zip->open($rootSourceDirPath . $sourceDirName . $extension, ZIPARCHIVE::CREATE) !== true) {
                return ['erreur' => "L'archive n'a pas pu etre crée"];
            }
        }

        if ($zip->addEmptyDir($zipRelativePath . $sourceDirName) === false) {
            return ['erreur' => "Le sous-dossier n'a pas pu etre crée"];
        }

        $dir = opendir($rootSourceDirPath . "/" . $sourceDirName);
        while ($entry = @readdir($dir)) {
            if ($entry !== ".." && $entry !== "." && is_dir($rootSourceDirPath . "/" . $sourceDirName . "/" . $entry)) {
                $res = $this->ZipDirectory(
                    $rootSourceDirPath . "/" . $sourceDirName,
                    $entry,
                    $zip,
                    $zipRelativePath . $sourceDirName . "/"
                );
                if (!$res) {
                    closedir($dir);
                    return ['erreur' => "Le sous-dossier n'a pas pu etre crée", 'status' => 404];;
                }
            } else {
                if ($entry !== "." && $entry !== ".." && !in_array($entry, $tabFilesIgnore, true)) {
                    $zip->addFile(
                        $rootSourceDirPath . "/" . $sourceDirName . "/" . $entry,
                        $zipRelativePath . $sourceDirName . "/" . $entry
                    );
                }
            }
        }
        closedir($dir);

        if ($zipRelativePath === "") {
            $zip->close();
        }

        return [
            'realPath' => $sourceDirName . $extension,
            "name" => $sourceDirName
        ];
    }

    /**
     * @param string $dirname nom du dossier temporaire principal
     * @return bool
     */
    public function removeDir(string $dirname): bool
    {
        $dirname = $this->directory . $dirname;

        if (is_dir($dirname)) {
            $dir = new RecursiveDirectoryIterator($dirname, FilesystemIterator::SKIP_DOTS);
            foreach (new RecursiveIteratorIterator($dir, RecursiveIteratorIterator::CHILD_FIRST) as $object) {
                if ($object->isFile()) {
                    unlink($object->getRealPath());
                } elseif ($object->isDir()) {
                    rmdir($object->getRealPath());
                } else {
                    return ['erreur' => 'Unknown object type: ' . $object->getFileName(), 'status' => 404];
                }
            }
            rmdir($dirname);
            return true;
        }

        return false;
    }

    /**
     * @param Download $download
     * @return BinaryFileResponse|bool
     */
    public function getFile(Download $download): BinaryFileResponse|bool
    {
        try {
            $displayName = basename($download->getFile());
            $fileName = $download->getFile();
            if($fileName === null){
                return false;
            }
            $file_with_path = $this->directory . $fileName;
            $response = new BinaryFileResponse ($file_with_path);
            $response->headers->set('Content-Type', 'application/octet-stream');
            $response->headers->set('Content-Description', 'File Transfer');
            $response->headers->set('Content-Transfer-Encoding', 'binary');
            $response->headers->set('Content-Length', filesize($file_with_path));
            $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $displayName);
            return $response;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param string $directory
     * @param string $strMainTeleDir
     * @return true
     */
    public function deleteFolder(string $directory, string $strMainTeleDir)
    {

        if(is_dir($directory . $strMainTeleDir)){
            rmdir($directory . $strMainTeleDir);
        }

        return true;
    }

    /**
     * @param $dirPath
     * @return void
     */
    function deleteDirectory($dirPath): void
    {
        $iterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($dirPath,
                FilesystemIterator::SKIP_DOTS),
            RecursiveIteratorIterator::CHILD_FIRST
        );
        foreach ($iterator as $file) {
            if ($file->isDir()) {
                rmdir($file->getPathname());
            } else {
                unlink($file->getPathname());
            }
        }
        rmdir($dirPath);
    }
}