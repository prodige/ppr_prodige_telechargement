<?php

namespace App\Service;

use App\Entity\Download;
use Prodige\ProdigeBundle\Services\AdminClientService;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Contracts\Translation\TranslatorInterface;

class MailerService
{
    public function __construct(
        private TranslatorInterface $translator,
        private AdminClientService $adminClientService,
        private ParameterBagInterface $parameterBag
    ) {
    }
    public function sendMail(MailerInterface $mailer,Download $download)
    {
        $url = $this->parameterBag->get('PRODIGE_URL_ADMIN') . "/api/settings?page=1&itemsPage=30&name=PRO_CATALOGUE_EMAIL_AUTO";
        $response = $this->adminClientService->curlAdminWithAdmincli($url, 'GET');
        if(array_key_exists('hydra:member',$response)){
            $email = $response['hydra:member'][0]['value'];
        }else{
            $email = 'noMail';
        }

        $mail = (new TemplatedEmail())
            ->from(new Address($email, 'Prodige_v5'))
            ->to($download->getUserMail())
            ->context(['download' => $download]);

        if ($download->getStatus()->getName() === "SUCCESS"){
            $mail->subject($this->translator->trans('Your download'));
            $mail->htmlTemplate('Mailer/success_mailer.html.twig');
        }else{
            $mail->subject($this->translator->trans('Error during download'));
            $mail->htmlTemplate('Mailer/failed_mailer.html.twig');
        }

        $mailer->send($mail);
    }

}