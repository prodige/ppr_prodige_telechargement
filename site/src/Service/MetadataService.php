<?php

namespace App\Service;

use App\Entity\Download;
use Prodige\ProdigeBundle\Services\AdminClientService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class MetadataService
{
    public function __construct(
        private AdminClientService $adminClientService,
        private ParameterBagInterface $parameterBag
    ) {
    }


    public function getMetadataInfo(int $publicId)
    {
        $response = $this->adminClientService->curlAdminWithAdmincli(
            $this->parameterBag->get(
                'ADMIN_URL_API'
            ) . '/metadata_sheets?publicMetadataId=' . $publicId,
            'GET'
        );

        if (array_key_exists("hydra:totalItems", $response) && $response["hydra:totalItems"] > 0) {

            $metadata = $response["hydra:member"][0];

            $resultat = [];

            $resultat['layers'] = $metadata['layers'];

            return $resultat;
        }

        return ['error' => 'No results found'];
    }
}