<?php

namespace App\Service;

use App\Entity\Download;
use App\Entity\Status;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class PreSaveInspectionService
{


    private array $vectorFormat;
    private array $tabulaireFormat;
    private array $rasterFormat;

    public function __construct(
        private ParameterBagInterface $parameterBag,
        private EntityManagerInterface $entityManager,
        private RightsService $rightsService
    ) {
        $this->tabulaireFormat = explode(',', $parameterBag->get('TABULAR_OUTPUT_EXTENSION'));
        $this->vectorFormat = explode(',', $parameterBag->get('VECTOR_OUTPUT_EXTENSION'));
        $this->rasterFormat = explode(',', $parameterBag->get('RASTER_OUTPUT_EXTENSION'));
    }

    /**
     * @param Download $download
     * @return array
     */
    public function checkingRequest(Download $download): array
    {
        $right = $this->rightsService->getRightAction($download->getMetadataUuid());

        if (array_key_exists('error', $right)) {
            return $right;
        }

        //on verifie que la donnée est téléchargeable
        $downloadable = $this->canBeDownloaded($download, $right);

        if (array_key_exists('error', $downloadable)) {
            return $downloadable;
        }

        // on controle la compatibilité des formats
        $format = $this->verifyFormat($download, $right);

        if (array_key_exists('error', $format)) {
            return $format;
        }

        // on controle les données dans extract
        $extract = $this->verifyExtract($download);

        if (array_key_exists('error', $extract)) {
            return $extract;
        }

        return [
            'data' => ['isDownloadable' => $downloadable['canDownloadData'], 'isValid' => $format]
        ];
    }

    /**
     * @param Download $download
     * @param array $right
     * @return array
     */
    public function canBeDownloaded(Download $download, array $right): array
    {
        if ($right['canDownloadData']) {
            if ($download->getStatus() === null) {
                if ($download->isDirect() === true) {
                    $status = $this->entityManager->find(Status::class, 2);
                } else {
                    $status = $this->entityManager->find(Status::class, 1);
                }
                $download->setStatus($status);
            }
            return ['canDownloadData' => true, 'status' => $status->getName()];
        } else {
            return ['erreur' => "Data cannot be downloaded"];
        }
    }

    /**
     * @param Download $download
     * @param array $right
     * @return array
     */
    public function verifyFormat(Download $download, array $right): array
    {
        //[1 = vecteur, 2 = rasteur, 3 = tabulaire, 4 = vue]
        switch ($right['data']['couche_type']) {
            case 3:
                $str = 'isVector';
                $validFormat = $this->tabulaireFormat;
                break;
            case 1:
                $str = 'isTabulaire';
                $validFormat = $this->vectorFormat;
                break;
            case 2:
                $str = 'isRaster';
                $validFormat = $this->rasterFormat;
                break;
        }

        if (in_array($download->getFormat(), $validFormat, true)) {
            return ['data' => [$str => true]];
        }

        return ['error' => "Input and output formats are not compatible"];
    }

    /**
     * @param Download $download
     * @return array
     */
    public function verifyExtract(Download $download): array
    {
        if ($download->getExtract() !== []) {
            switch ($download->getExtract()['type']) {
                case 'list':
                    if (isset($download->getExtract()['area_type_id'], $download->getExtract()['area_id'])) {
                        if (gettype($download->getExtract()['area_type_id']) !== "integer") {
                            return ['error' => "Aera type id must be a integer"];
                        }
                        if (gettype($download->getExtract()['area_id']) !== "array") {
                            return ['error' => "Aera id must be a array"];
                        }
                        if (empty($download->getExtract()['area_id'])) {
                            return ['error' => "Area id must have at least one data item"];
                        }
                        foreach ($download->getExtract()['area_id'] as $areaId) {
                            if (gettype($areaId) !== "string") {
                                return ['error' => "each element of aera id must be a string"];
                            }
                        }
                    }else{
                        return ['error' => "incomplete data sets, area type id or area id is not present"];
                    }
                    break;
                case 'polygon':
                    if (isset($download->getExtract()['area'])) {
                        if (gettype($download->getExtract()['area']) !== "array") {
                            return ['error' => "Aera must be a array"];
                        }
                        if (!isset($download->getExtract()['area']['coordinates'])) {
                            return ['error' => "incomplete data sets, area.coordinates is not present"];
                        }
                        if (gettype($download->getExtract()['area']['coordinates']) !== "array") {
                            return ['error' => "Coordinates must be a array"];
                        }
                        foreach ($download->getExtract()['area']['coordinates'] as $polygon) {
                            foreach ($polygon as $coordinates) {
                                foreach ($coordinates as $value) {
                                    if (gettype($value) !== "double") {
                                        return ['error' => "each element of Coordinates must be a double"];
                                    }
                                }
                            }
                        }
                    }else{
                        return ['error' => "incomplete data sets, area is not present"];
                    }
                    break;
            }
            return ['data' => 'Extract is complete'];
        }
        return ['data' => 'download without extract'];
    }
}
