<?php

namespace App\Service;

use Prodige\ProdigeBundle\Controller\VerifyRightsController;
use Prodige\ProdigeBundle\Services\AdminClientService;
use Prodige\ProdigeBundle\Services\CurlUtilities;
use Prodige\ProdigeBundle\Services\UserService;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\HttpFoundation\Request;

class RightsService
{

    public function __construct(
        private VerifyRightsController $verifyRightsController,
        private UserService $userService,
        private AdminClientService $adminClientService,
        private ParameterBagInterface $parameterBag
    ) {
    }

    /**
     * @param Uuid $uuid
     * @return array
     */
    public function getRightAction(Uuid $uuid): array
    {
        $options = 'TRAITEMENTS=TELECHARGEMENT%7CNAVIGATION&OBJET_TYPE=dataset&uuid=' . $uuid;
        $right = $this->adminClientService->curlAdmin(
            $this->parameterBag->get('ADMIN_URL_API') . '/internal/verify_right?' . $options,
            'GET'
        );

        if (is_array($right) && array_key_exists('error', $right)) {
            return [
                'error' => "ERREUR : vos droits n'ont pas eu être récupérer",
                'success' => false
            ];
        }

        if (isset($right['right']['TELECHARGEMENT'])) {
            if($right['right']['userNom'] === 'Internet' && $right['right']['TELECHARGEMENT'] === false){
                $candowload = false;
            }else{
                $candowload = true;
            }
            //TODO: gerer le cas de vinternet pour les droit de la métadonnées
            $response = [
                'canDownloadData' => $candowload,
                'publicId' => $right['right']['fmeta_id'],
                'data' => [],
                'user' => [
                    'userId' => $right['right']['userId'],
                    'userNom' => $right['right']['userNom'],
                    'userPrenom' => $right['right']['userPrenom'],
                    'userLogin' => $right['right']['userLogin'],
                    'userEmail' => $right['right']['userEmail'],
                    'isAdmProdige' => $right['right']['isAdmProdige'],
                    'isConnected' => $right['right']['isConnected']
                ],
                'success' => $right['right']['success']
            ];
            unset(
                $right['right']['TELECHARGEMENT'], $right['right']['fmeta_id'], $right['right']['success'],
                $right['right']['userId'], $right['right']['userNom'], $right['right']['userPrenom'],
                $right['right']['userLogin'], $right['right']['userEmail'],
                $right['right']['isAdmProdige'], $right['right']['isConnected']
            );
            $response['data'] = $right['right'];
        } else {
            if (!array_key_exists('fmeta_id', $right['right'])) {
                $response = [
                    'error' => "ERREUR : la donnée " . $uuid . " n'existe pas",
                    'success' => false
                ];
            } else {
                $response = [
                    'error' => "ERREUR : vos droits ne vous permettent pas d'accéder à la donnée " . $uuid,
                    'status' => 403
                ];
            }
        }
        return $response;
    }

    public function getMe()
    {
        $url = $this->parameterBag->get('PRODIGE_URL_ADMIN') . "/api/internal/me";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/ld+json'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
        $response = curl_exec($curl);
        $me = json_decode($response, true);

        curl_close($curl);
        if (is_array($me)) {
            return $me;
        }
        return false;
    }

    public function getUserInfo($user)
    {
        $url = $this->parameterBag->get('PRODIGE_URL_ADMIN') . "/api/users/" . $user->getId();
        $response = $this->adminClientService->curlAdminWithAdmincli($url, 'GET');

        if (is_array($response)) {
            return $response;
        }
        return false;
    }
}