<?php

namespace App\Service;

use Prodige\ProdigeBundle\Services\CurlUtilities;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Uid\Uuid;

class InfoService
{
    public function __construct(
        private LoggerInterface $logger,
        private ParameterBagInterface $parameterBag,
    ) {
    }

    /**
     * @param Uuid $uuid
     * @return array
     * @throws \Exception
     */
    public function getGeonetworkInfo(Uuid $uuid)
    {
        $this->logger->info('Get metadatasheet info to geonetwork');
        $url = $this->parameterBag->get('PRODIGE_URL_CATALOGUE') . '/geonetwork/srv/api/records/' . $uuid . '/formatters/json';
        $curl = CurlUtilities::curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
        $response = curl_exec($curl);
        $responseJson = json_decode($response, true);

        curl_close($curl);
        if ($responseJson === null) {
            $this->logger->error('curl metadatasheet info to geonetwork error ', ['error' => $response]);
            return ["error" => $response, "status" => 409];
        }
        if (is_array($responseJson) && array_key_exists("message", $responseJson)) {
            return ["error" => $responseJson['message'], "status" => 404];
        }

        return ["data" => $responseJson, "status" => 200];
    }

    public function getLayerInfo($id){
        $this->logger->info('Get Layer info to admin');
        $url = $this->parameterBag->get('PRODIGE_URL_ADMIN') . '/api/layers/' . $id;
        $curl = CurlUtilities::curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/ld+json'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
        $response = curl_exec($curl);
        $responseJson = json_decode($response, true);

        curl_close($curl);

        if ($responseJson === null) {
            $this->logger->error('curl layer info to admin error ', ['error' => $response]);
            return ["error" => $response, "status" => 409];
        }
        if (is_array($responseJson) && array_key_exists("message", $responseJson)) {
            return ["error" => $responseJson['message'], "status" => 404];
        }
        return ["data" => $responseJson, "status" => 200];
    }
}