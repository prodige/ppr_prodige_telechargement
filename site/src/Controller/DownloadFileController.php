<?php

namespace App\Controller;

use App\Entity\Download;
use App\Response\JsonLDResponse;
use App\Service\FileService;
use App\Service\RightsService;
use Doctrine\ORM\EntityManagerInterface;
use Prodige\ProdigeBundle\Services\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

#[AsController]
class DownloadFileController extends AbstractController
{
    public function __construct(
        private FileService $fileService,
        private Security $security,
        private EntityManagerInterface $entityManager,
        private RightsService $rightsService,
        private UserService $userService
    ) {
    }

    public function __invoke(int $id)
    {
        $user = $this->security->getUser();

        if ($user->getId() === 0) {
            $user = $this->rightsService->getMe();
            $initUser = $this->userService->initUser($user['login']);
            $user = $this->userService->getUser();
        }

        $download = $this->entityManager->getRepository(Download::class)->findOneBy(
            ['id' => $id, 'userId' => $user->getId()]
        );

        if (!empty($download)) {
            $response = $this->fileService->getFile($download);

            if($response === false){
                return new JsonLDResponse (['error' => "Download failed :" . $download->getId()], 400);
            }

            return $response;
        }

        return new JsonLDResponse(['error' => "ERREUR : vos droits ne vous permettent pas d'accéder à la donnée "],
            403,
            [],
            false,
            "Download");
    }
}
