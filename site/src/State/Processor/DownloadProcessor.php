<?php

declare(strict_types=1);

namespace App\State\Processor;

use ApiPlatform\Metadata\DeleteOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\Download;
use App\Message\DiffereImportMessage;
use App\Message\DirectImportMessage;
use App\Response\JsonLDResponse;
use App\Service\ImportQueueService;
use App\Service\PreSaveInspectionService;
use App\Service\RightsService;
use Prodige\ProdigeBundle\Services\UserService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Security\Core\Security;
use Prodige\ProdigeBundle\Services\Logs;

class DownloadProcessor implements ProcessorInterface
{

    public function __construct(
        private ProcessorInterface $persistProcessor,
        private ProcessorInterface $removeProcessor,
        private Security $security,
        private MessageBusInterface $messageBus,
        private ImportQueueService $queueService,
        private RightsService $rightsService,
        private PreSaveInspectionService $inspectionService,
        private UserService $userService,
        private Logs $logs,
        private ContainerInterface $container
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function process($data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        /** @var Download $data */
        $user = $this->security->getUser();

        if ($user === null) {
            throw new BadRequestHttpException(
                "La récuperation de l'utilisatuer a échoué",
                null,
                404
            );
        }
        if ($user->getId() === 0) {
            $user = $this->rightsService->getMe();
            $initUser = $this->userService->initUser($user['login']);
            $user = $this->userService->getUser();
        }


        $isAdmin = false;
        if ($this->security->getUser()->getRoles()) {
            if (in_array('ROLE_ADMIN', $this->security->getUser()->getRoles())) {
                $isAdmin = true;
            }
        }


        if (($operation instanceof DeleteOperationInterface) && $isAdmin === true) {
            if ($data->getStatus()->getName() !== "SUCCESS") {
                $deleteInQueue = $this->queueService->deleteInQueue($data);
                if ($deleteInQueue === false) {
                    throw new BadRequestHttpException(
                        "echec de la suppression de l'ordre de téléchargement",
                        null, 404
                    );
                }
            }

            return $this->removeProcessor->process($data, $operation, $uriVariables, $context);
        }

        $data->setUserId($user->getId());

        if ($data->getUserMail() === null) {
            $userInfo = $this->rightsService->getUserInfo($user);
            $data->setUserMail($userInfo['email']);
        }

        $checkingRequest = $this->inspectionService->checkingRequest($data);

        if (array_key_exists('error', $checkingRequest)) {
            return new JsonLDResponse(['error' => $checkingRequest['error']],
                422,
                [],
                false,
                "Download");
        }

        if (array_key_exists('data', $checkingRequest)) {
            if ($data->isDirect() === true) {
                $data->setQueueName('direct');
            } else {
                $data->setQueueName('differed');
            }

            $this->persistProcessor->process($data, $operation, $uriVariables, $context);

            /** @var Envelope $messengerResponse */
            if ($data->isDirect() === true) {
                $messengerResponse = $this->messageBus->dispatch(new DirectImportMessage($data->getId()));
            } else {
                $messengerResponse = $this->messageBus->dispatch(new DiffereImportMessage($data->getId()));
            }
            if (!empty($messengerResponse->all())) {
                $data->setQueueId(
                    (int)$messengerResponse->last('Symfony\Component\Messenger\Stamp\TransportMessageIdStamp')->getId()
                );
            }
            $rigthsData = $this->rightsService->getRightAction($data->getMetadataUuid());


            if(isset($userInfo)){
                $text = [$userInfo['name'],
                    $userInfo['firstName'],
                    $userInfo['login'],
                    $data->getMetadataUuid(),
                    $rigthsData['publicId'],
                    $data->getFormat(),
                    str_replace('EPSG:', '', $data->getSrs()) ];
            }else{
                $text = [
                    $user->getName(),
                    $user->getFirstName(),
                    $data->getUserMail() !== null ? $data->getUserMail() : 'Demande sans adresse mail',
                    $data->getMetadataUuid(),
                    $rigthsData['publicId'],
                    $data->getFormat(),
                    str_replace('EPSG:', '', $data->getSrs())];
            }
            $addLogs = new Logs();
            $addLogs->setContainer($this->container);
            // ajout des logs de telechargement
            $addLogs->AddLogs('telechargements', $text);
             
            return $this->persistProcessor->process($data, $operation, $uriVariables, $context);
        }
        throw new BadRequestHttpException(
            "La demande n'a pas été traité",
            null,
            404
        );
    }

}