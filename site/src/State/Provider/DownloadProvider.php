<?php

declare(strict_types=1);

namespace App\State\Provider;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;

class DownloadProvider implements ProviderInterface
{
    public function __construct(
        private ProviderInterface $itemProvider
    ) {
    }

    /**
     * @inheritDoc
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
    {
        $download = $this->itemProvider->provide($operation, $uriVariables, $context);
        // TODO: Implement provide() method.
        return $download;
    }
}