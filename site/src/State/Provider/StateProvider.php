<?php

namespace App\State\Provider;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Controller\DownloadFileController;
use App\Entity\Download;
use App\Response\JsonLDResponse;
use App\Service\FileService;
use App\Service\RightsService;
use Prodige\ProdigeBundle\Services\UserService;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Mime\FileinfoMimeTypeGuesser;

class StateProvider implements ProviderInterface
{
    private string $directory;

    public function __construct(
        private ProviderInterface $itemProvider,
        private ParameterBagInterface $parameterBag,
        private RightsService $rightsService,
        private UserService $userService,
        private Security $security,
        private FileService $fileService
    ) {
        $this->directory = $this->parameterBag->get('TELECHARGEMENT_DOWNLOAD_PATH');
    }

    /**
     * @inheritDoc
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
    {
        $userId = $this->security->getUser()->getId();

        if ($userId === 0) {
            $user = $this->rightsService->getMe();
            $initUser = $this->userService->initUser($user['login']);
            $userId = $this->userService->getUser()->getId();
        }

        $downloadState = $this->itemProvider->provide($operation, $uriVariables, $context);

        if($downloadState !== null){
            /** @var Download $downloadState */

            if($userId === $downloadState->getUserId()){
                return $downloadState;
            }
            return New JsonLDResponse(['error' => "ERREUR : vos droits ne vous permettent pas d'accéder à la donnée " . $downloadState->getMetadataUuid()],403,[] ,false,"Download", $downloadState->getId() );
        }

        return New JsonLDResponse(['error' => "Data not found :" . $uriVariables['id']],403,[] ,false,"Download");
    }

}