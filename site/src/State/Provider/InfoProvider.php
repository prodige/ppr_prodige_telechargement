<?php

namespace App\State\Provider;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\Info;
use App\Entity\ResourceConstraints;
use App\Response\JsonLDResponse;
use App\Service\MetadataService;
use Exception;
use Prodige\ProdigeBundle\Services\UserService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Service\InfoService;
use App\Service\RightsService;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Uid\Uuid;
use Prodige\ProdigeBundle\Services\CurlUtilities;


class InfoProvider implements ProviderInterface
{
    private array $refSpatialRepresentation = ['1' => 'vecteur', '2' => 'raster', '3' => 'tabulaire', '4', 'vue'];

    public function __construct(
        private RightsService $rightsService,
        private ParameterBagInterface $parameterBag,
        private MetadataService $metadataSheetService,
        private InfoService $infoService,
        private Security $security,
        private UserService $userService
    ) {
    }

    /**
     * @inheritDoc
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
    {
        if (array_key_exists('uuid', $uriVariables) && Uuid::isValid($uriVariables['uuid'])) {
            $user = $this->security->getUser();

            if ($user === null) {
                throw new BadRequestHttpException(
                    "La récuperation de l'utilisatuer a échoué",
                    null,
                    404
                );
            }
            if ($user->getId() === 0) {
                $user = $this->rightsService->getMe();
                $initUser = $this->userService->initUser($user['login']);
                $user = $this->userService->getUser();
            }

            $uuid = Uuid::fromString($uriVariables['uuid']);
            $right = $this->rightsService->getRightAction($uuid);

            if (isset($right['canDownloadData']) && $right['canDownloadData'] === true) {
                $metadataInfo = $this->infoService->getGeonetworkInfo($uuid);
                if (!array_key_exists('data', $metadataInfo)) {
                    throw new AccessDeniedHttpException("Accès refusé", null, 403);
                }

                $layerInfo = $this->infoService->getLayerInfo($right['data']['pk_data']);
                $metadataTitle = $metadataInfo['data']['gmd:identificationInfo']['gmd:MD_DataIdentification']['gmd:citation']['gmd:CI_Citation']['gmd:title']['gco:CharacterString']['#text'];

                //take layer default representation, or default extract map if exists
                $extractMap = '';
                if (file_exists(
                    $this->parameterBag->get('PRODIGE_PATH_DATA') . '/cartes/Publication/layers/' . $uuid . '.map'
                )) {
                    $extractMap = $this->parameterBag->get(
                            'PRODIGE_URL_FRONTCARTO'
                        ) . '/carto/context?account=1&contextPath=/layers/' . $uuid . '.map';
                } else {
                    $map = $this->getMapModele('Extraction');
                    if ($map) {
                        $extractMap = $this->parameterBag->get('PRODIGE_URL_FRONTCARTO') . '/1/' . $map;
                    }
                }

                if (isset($metadataInfo['data']['gmd:identificationInfo']['gmd:MD_DataIdentification']['gmd:resourceConstraints'])) {
                    if (count(
                            $metadataInfo['data']['gmd:identificationInfo']['gmd:MD_DataIdentification']['gmd:resourceConstraints']
                        ) === 1) {
                        foreach ($metadataInfo['data']['gmd:identificationInfo']['gmd:MD_DataIdentification']['gmd:resourceConstraints'] as $key => $constraint) {
                            if ($key === 'gmd:MD_Constraints' && isset($constraint['gmd:useLimitation'])) {
                                $useLimitations[] = $constraint['gmd:useLimitation']['gco:CharacterString'];
                            } else {
                                $useLimitations = null;
                            }
                        }

                        foreach ($metadataInfo['data']['gmd:identificationInfo']['gmd:MD_DataIdentification']['gmd:resourceConstraints'] as $key => $constraint) {
                            if ($key === 'gmd:MD_LegalConstraints') {
                                if (array_key_exists('gmd:otherConstraints', $constraint)) {
                                    $otherConstraints = $constraint['gmd:otherConstraints'];
                                }

                                if(array_key_exists('gmd:useLimitation', $constraint)) {
                                    $useLimitations = $constraint['gmd:useLimitation'];
                                }
                            }
                        }

                        $constraintString = $otherConstraints['gco:CharacterString']['#text'] ?? null;
                    } else {
                        if (isset($metadataInfo['data']['gmd:identificationInfo']['gmd:MD_DataIdentification']['gmd:resourceConstraints'])) {
                            foreach ($metadataInfo['data']['gmd:identificationInfo']['gmd:MD_DataIdentification']['gmd:resourceConstraints'] as $constraint) {
                                if (isset($constraint['gmd:MD_LegalConstraints'])) {
                                    foreach ($constraint['gmd:MD_LegalConstraints'] as $key => $constraintLegal) {
                                        switch ($key) {
                                            case "gmd:useLimitation":
                                                foreach ($constraintLegal as $useLimitation) {
                                                    $useLimitations[] = $useLimitation;
                                                }
                                                break;
                                            case "gmd:otherConstraints":
                                                $arrayOther = [];
                                                foreach ($constraintLegal as $other) {
                                                    if (isset($other['#text'])) {
                                                        $text = $other['#text'];
                                                        $arrayOther[] = $text;
                                                    }
                                                }
                                                $constraintString = implode(" .", $arrayOther);
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }


                    $arrayLimitations = [];

                    if (isset($useLimitations) && $useLimitations !== null) {
                        foreach ($useLimitations as $limitation) {
                            $text = $limitation['#text'];
                            $arrayLimitations[] = $text;
                        }
                    }
                }
                $adminInfo = $this->metadataSheetService->getMetadataInfo($right['publicId']);

                if (array_key_exists('error', $adminInfo)) {
                    throw new BadRequestHttpException(
                        $adminInfo['error'],
                        null,
                        404
                    );
                }

                $layers = [];
                foreach ($adminInfo['layers'] as $layerAdmin) {
                    $layers[] = [
                        'storagePath' => $layerAdmin['storagePath'],
                        'name' => $layerAdmin['name']
                    ];
                }

                $metadataSheet = new Info();
                $metadataSheet->setUuid($uuid);
                $metadataSheet->setMetadataTitle($metadataTitle);
                $metadataSheet->setExtractMap($extractMap);
                $metadataSheet->setSpatialRepresentationType(
                    $this->refSpatialRepresentation[$right['data']['couche_type']]
                );
                $metadataSheet->setLayers($layers);
                $metadataSheet->setCguDisplay($layerInfo['data']['cguDisplay']);
                $metadataSheet->setCguMessage($layerInfo['data']['cguMessage']);
                if (isset($arrayLimitations) || isset($constraintString)) {
                    $constraints = new ResourceConstraints();
                    if (isset($arrayLimitations)) {
                        $constraints->setUseLimitation($arrayLimitations);
                    }
                    if (isset($constraintString)) {
                        $constraints->setOtherConstraints($constraintString);
                    }
                    $metadataSheet->setResourceConstraints($constraints);
                } else {
                    $constraints = new ResourceConstraints();
                    $metadataSheet->setResourceConstraints($constraints);
                }
                // TODO: Implement provide() method.
                return $metadataSheet;
            } else {
                throw new AccessDeniedHttpException("La donnée n'est pas téléchargeable:" . $uuid, null, 403);
            }
        }

        throw new BadRequestHttpException('Parametre uuid manquant ou invalid', null, 404);
    }

    public function getMapModele(string $type)
    {
        $url = $this->parameterBag->get('PRODIGE_URL_ADMIN') . "/api/templates?lexTemplateType.name=" . $type;
        $curl = CurlUtilities::curl_init($url);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/ld+json'));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        $responseGet = json_decode($response, true);
        curl_close($curl);


        if ($responseGet["hydra:totalItems"] === 0) {
            return false;
        }
        $map = $responseGet["hydra:member"][0]['mapfile'];
        return $map;
    }


}
