<?php

namespace App\Message;


class DirectImportMessage
{
    private int $downloadId;

    public function __construct( int $downloadId)
    {
        $this->downloadId = $downloadId;
    }

    /**
     * @return int
     */
    public function getDownloadId(): int
    {
        return $this->downloadId;
    }

}