<?php

declare(strict_types=1);

namespace App\Message;


class DiffereImportMessage
{

    private int $downloadId;

    public function __construct( int $downloadId)
    {
        $this->downloadId = $downloadId;
    }

    /**
     * @return int
     */
    public function getDownloadId(): int
    {
        return $this->downloadId;
    }

}