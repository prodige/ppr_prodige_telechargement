<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230523112747 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Creation of messenger_message tables';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql(
            'CREATE SEQUENCE IF NOT EXISTS telechargement.messenger_messages_direct_import_id_seq INCREMENT BY 1 MINVALUE 1 START 1'
        );
        $this->addSql(
            'CREATE SEQUENCE IF NOT EXISTS  telechargement.messenger_messages_differed_import_id_seq INCREMENT BY 1 MINVALUE 1 START 1'
        );
        $this->addSql(
            'CREATE TABLE  IF NOT EXISTS  telechargement.messenger_messages_differed_import (id BIGSERIAL NOT NULL, body TEXT NOT NULL, headers TEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, available_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, delivered_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))'
        );
       
        $this->addSql(
            'CREATE TABLE IF NOT EXISTS  telechargement.messenger_messages_direct_import (id BIGSERIAL NOT NULL, body TEXT NOT NULL, headers TEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, available_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, delivered_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))'
        );
        $this->addSql(
            'CREATE INDEX IF NOT EXISTS  idx_563a286d16ba31db ON telechargement.messenger_messages_direct_import (delivered_at)'
        );
        $this->addSql(
            'CREATE INDEX IF NOT EXISTS  idx_563a286de3bd61ce ON telechargement.messenger_messages_direct_import (available_at)'
        );
        $this->addSql(
            'CREATE INDEX IF NOT EXISTS  idx_563a286dfb7336f0 ON telechargement.messenger_messages_direct_import (queue_name)'
        );
      
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE telechargement.messenger_messages_direct_import_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE telechargement.messenger_messages_differed_import_id_seq CASCADE');
        $this->addSql('DROP TABLE telechargement.messenger_messages_differed_import');
        $this->addSql('DROP TABLE telechargement.messenger_messages_direct_import');
    }
}
