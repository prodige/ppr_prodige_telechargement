<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230523112641 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Injection of status into the database';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql(
            "INSERT INTO telechargement.status (id, name, description) values (1, 'PLANNED','The download has been put in the delayed download queue'),
                 (2, 'IN_PROGRESS','The download has been placed in the immediate download queue'),  (3, 'SUCCESS','The download has been successfully completed'),
                 (4, 'FAILED','An error makes it impossible to download')"
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("TRUNCATE TABLE telechargement.status");
    }
}
