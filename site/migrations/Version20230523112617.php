<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230523112617 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Creation of the schema and setting up of the entities';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA IF NOT EXISTS telechargement');
        $this->addSql('CREATE TABLE telechargement.download (id SERIAL NOT NULL, status_id INT NOT NULL, metadata_uuid UUID NOT NULL, user_id INT NOT NULL, user_mail VARCHAR(128) NOT NULL, direct BOOLEAN NOT NULL, format VARCHAR(128) NOT NULL, srs VARCHAR(128) NOT NULL, extract JSON NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3BF4F39E6BF700BD ON telechargement.download (status_id)');
        $this->addSql('COMMENT ON COLUMN telechargement.download.metadata_uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE telechargement.status (id SERIAL NOT NULL, name VARCHAR(128) NOT NULL, description VARCHAR(128) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE telechargement.download ADD CONSTRAINT FK_3BF4F39E6BF700BD FOREIGN KEY (status_id) REFERENCES telechargement.status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SCHEMA telechargement');

    }
}
