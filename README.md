# project

## Context

Template for Symfony >= 6 with php 8.2

## Rename template

To rename the template to your projet name, run this :

```bash
./cicd/update-name.sh
```

## Development

All informations for development are in [cicd/dev/README.md](cicd/dev/README.md)

## Build for test and production

All informations for build for test and production are in [cicd/build/README.md](cicd/build/README.md)

## History of steps to construct this project

This part is just information to understand how this project was constructed

Build container and init the Symfony project.

```bash
cd ./cicd/dev/
docker login docker.alkante.com
# Put UID and GID in env file
echo -e "LOCAL_USER_ID=$(id -u)\nLOCAL_GROUP_ID=$(id -g)" > docker-compose.env;
# Build dev image
docker-compose build
# Run containers
docker-compose up
```

In a 2nd terminal :  
```bash
docker exec --user www-data -w /var/www/html/ -it project_dev_web bash
```

https://wiki.alkante.al/langages/symfony/Installer-un-nouveau-projet-Symfony#pr%C3%A9paration-du-projet  

Choose one of this :  

Symfony 5  
```bash
composer create-project symfony/website-skeleton:"^5.4" site
```

Symfony 6
```bash
composer create-project symfony/website-skeleton:"^6.2" site
```

Change access variable to database in `site/.env`  
```
DATABASE_URL=postgresql://user_test:user_pass@db:5432/test?serverVersion=12&charset=utf8
```

Create migration file to initialize database
```bash
./site/bin/console doctrine:migrations:generate
```

Edit it `site/src/Migrations/Version20200221092441.php`

Create default controller

```bash
./site/console make:controller BlogController
```

Edit it `site/src/Controller/DefaultController.php`.

Create template used by this controller `site/templates/default/user.html.twig`.

Create `.gitignore`.

```bash
echo "/jenkins_release" >> ./.gitignore
```

Finished. :)

Restart all containers by following the dev procedure cf. [cicd/dev/README.md](cicd/dev/README.md)
